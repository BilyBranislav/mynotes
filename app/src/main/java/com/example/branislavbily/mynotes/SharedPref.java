package com.example.branislavbily.mynotes;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPref {

    private SharedPreferences sharedPreferences;

    public SharedPref(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public int getCheckedItem() {
        return sharedPreferences.getInt(PreferencesHelper.CHECKED_ITEM, 0);
    }

    public void setCheckedItem(int checkedItem) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(PreferencesHelper.CHECKED_ITEM, checkedItem);
        editor.commit();
    }
}
