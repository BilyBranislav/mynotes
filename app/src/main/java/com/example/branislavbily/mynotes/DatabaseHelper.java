package com.example.branislavbily.mynotes;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.time.LocalDate;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 04.12.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(Context context) {
        super(context, NoteModul.Note.DATABASE_NAME, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createTableQuery());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("Drop table IF EXISTS " + NoteModul.Note.TABLE_NAME);
        onCreate(db);
    }

    private String createTableQuery() {
        String sqlTemplate = "CREATE TABLE %s ("
                + "%s INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "%s TEXT,"
                + "%s TEXT,"
                + "%s DATETIME,"
                + "%s DATETIME"
                + ")";
        return String.format(sqlTemplate, NoteModul.Note.TABLE_NAME, "ID", NoteModul.Note.TITLE, NoteModul.Note.DESCRIPTION, NoteModul.Note.CREATED_ON, NoteModul.Note.MODIFIED_ON);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean insertData(String title, String description) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NoteModul.Note.TITLE, title);
        contentValues.put(NoteModul.Note.DESCRIPTION, description);
        contentValues.put(NoteModul.Note.CREATED_ON, LocalDate.now().toString());
        contentValues.put(NoteModul.Note.MODIFIED_ON, LocalDate.now().toString());
        long result = db.insert(NoteModul.Note.TABLE_NAME, null, contentValues);
        return result != -1;
    }

    public Cursor getAllData(String orderBy, boolean isAscending) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery(selectAllFromQuery(orderBy, isAscending), null);
    }

    private String selectAllFromQuery(String orderBy, boolean isAscending) {
        if(isAscending) {
            return "SELECT * FROM " + NoteModul.Note.TABLE_NAME + " ORDER BY "+ orderBy +" ASC";
        }
        return "SELECT * FROM " + NoteModul.Note.TABLE_NAME + " ORDER BY "+orderBy+" DESC";
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public boolean updateNote(String id, String title, String description) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("ID", id);
        contentValues.put(NoteModul.Note.TITLE, title);
        contentValues.put(NoteModul.Note.DESCRIPTION, description);
        contentValues.put(NoteModul.Note.MODIFIED_ON, LocalDate.now().toString());
        int result = db.update(NoteModul.Note.TABLE_NAME, contentValues, "id = ?", new String[] {id});
        if(result == 0) {
            return  false;
        } else {
            return true;
        }
    }

    public boolean deleteNote(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int result =  db.delete(NoteModul.Note.TABLE_NAME, "id = ?", new String[] {id});
        return result != 0;
    }

    public Cursor getNote(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery(createOneNoteQuery(), new String[] {id});
    }

    private String createOneNoteQuery() {
        return "SELECT * FROM notes_table WHERE id = ?";
    }
}