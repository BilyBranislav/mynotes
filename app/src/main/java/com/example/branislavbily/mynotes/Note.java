package com.example.branislavbily.mynotes;


/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 04.12.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */

public class Note {
    private String id;
    private String title;
    private String description;
    private String createdOn;
    private String modifiedOn;



    public Note(String id, String title, String description, String createdOn, String modifiedOn) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.createdOn = createdOn;
        this.modifiedOn = modifiedOn;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public String getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(title).append(" ").append(description).append(" ").append(createdOn).append(" ").append(modifiedOn);
        return sb.toString();
    }
}
