package com.example.branislavbily.mynotes.Activities;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.branislavbily.mynotes.DatabaseHelper;
import com.example.branislavbily.mynotes.R;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 04.12.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */

public class AddNewNoteActivity extends AppCompatActivity {

    DatabaseHelper myDb;

    EditText editTextTitle, editTextDescription;
    Button buttonSave;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_note);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        myDb = new DatabaseHelper(this);

        editTextTitle = findViewById(R.id.editTextTitle);
        editTextDescription = findViewById(R.id.editTextDescription);

        buttonSave = findViewById(R.id.buttonSave);

        buttonSave.setOnClickListener(view -> {
            String title = editTextTitle.getText().toString();
            String description = editTextDescription.getText().toString();
            if(!(title.equals("")) && !(description.equals(""))) {
                if (myDb.insertData(title, description)) {
                    finish();
            } else {
                    Toast.makeText(this, "Could not add Note!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Please insert all values!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
