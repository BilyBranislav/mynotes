package com.example.branislavbily.mynotes.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.branislavbily.mynotes.DatabaseHelper;
import com.example.branislavbily.mynotes.IntentModul;
import com.example.branislavbily.mynotes.Note;
import com.example.branislavbily.mynotes.NoteAdapter;
import com.example.branislavbily.mynotes.NoteModul;
import com.example.branislavbily.mynotes.R;
import com.example.branislavbily.mynotes.SharedPref;

import java.util.ArrayList;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 04.12.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */

public class MainActivity extends AppCompatActivity implements NoteAdapter.OnNoteClickListener {

    //TODO better design, better listView

    private DatabaseHelper myDB;
    private TextView textViewNoNotesMessage;
    private ImageView imageViewNoNotes;
    private int checkedItem;
    private SharedPref sharedPref;
    private RecyclerView recyclerView;

    private ArrayList<Note> notes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPref = new SharedPref(this);
        notes = new ArrayList<>();

        /* Allows to see the database in browser, great during development
        Stetho.initialize(Stetho.newInitializerBuilder(this)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                .build());*/

        myDB = new DatabaseHelper(this);

        textViewNoNotesMessage = findViewById(R.id.textViewNoNotesMessage);
        imageViewNoNotes = findViewById(R.id.imageView);

        loadNotes();

        FloatingActionButton fab =  findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            startActivity(new Intent(MainActivity.this, AddNewNoteActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        });
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void finish() {
        super.finish();
    }

    private void loadNotes() {
        checkedItem = sharedPref.getCheckedItem();
        Cursor result;
        switch (checkedItem) {
            case 1: result = myDB.getAllData(NoteModul.Note.MODIFIED_ON, true);break;
            case 2: result = myDB.getAllData(NoteModul.Note.MODIFIED_ON, false);break;
            case 3: result = myDB.getAllData(NoteModul.Note.CREATED_ON, true);break;
            case 4: result = myDB.getAllData(NoteModul.Note.CREATED_ON, false);break;
            default: result = myDB.getAllData(NoteModul.Note.TITLE, false);
        }

        if(result.getCount() > 0) {
            textViewNoNotesMessage.setVisibility(View.INVISIBLE);
            imageViewNoNotes.setVisibility(View.INVISIBLE);
            notes.clear();
            while (result.moveToNext()) {
                notes.add(loadNoteFromDatabase(result));
            }
            recyclerView = findViewById(R.id.recyclerView);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            NoteAdapter adapter = new NoteAdapter(this, notes);
            recyclerView.setAdapter(adapter);
        } else {
            textViewNoNotesMessage.setVisibility(View.VISIBLE);
            imageViewNoNotes.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.sortOptionMenu) {
            onSortOptionMenuSelected();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadNotes();

        NoteAdapter adapter = new NoteAdapter(this, notes);
        recyclerView.setAdapter(adapter);
    }

    private void onSortOptionMenuSelected() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        String[] choices = createChoices();

        builder.setSingleChoiceItems(choices, checkedItem, (dialog, which) -> {
            switch (which) {
                case 0: {
                    sharedPref.setCheckedItem(0);
                    loadNotes();
                    dialog.cancel();break;
                }
                case 1: {
                    sharedPref.setCheckedItem(1);
                    loadNotes();
                    dialog.cancel();break;
                }
                case 2: {
                    sharedPref.setCheckedItem(2);
                    loadNotes();
                    dialog.cancel();break;
                }
                case 3: {
                    sharedPref.setCheckedItem(3);
                    loadNotes();
                    dialog.cancel();break;
                }
                case 4: {
                    sharedPref.setCheckedItem(4);
                    loadNotes();
                    dialog.cancel();break;
                }
            }
        });

        builder.setTitle(getResources().getString(R.string.sortBy));
        builder.setNeutralButton("Cancel", (dialog, which) -> dialog.cancel());

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private String[] createChoices() {
        return new String [] {
                "Title",
                "Date modified (oldest)",
                "Date modified (most recent)",
                "Date created (oldest)",
                "Date created (most recent)"
        };
    }

    private Note loadNoteFromDatabase(Cursor result) {
        return new Note(result.getString(0), result.getString(1),
                result.getString(2), result.getString(3),
                result.getString(4));
    }

    @Override
    public void onNoteClick(String noteId) {
        Intent intent = new Intent(MainActivity.this, NoteActivity.class);
        intent.putExtra(IntentModul.NOTE_ID, noteId);
        startActivity(intent);

    }
}
