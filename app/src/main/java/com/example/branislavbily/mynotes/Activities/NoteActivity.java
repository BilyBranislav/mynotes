package com.example.branislavbily.mynotes.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.branislavbily.mynotes.IntentModul;
import com.example.branislavbily.mynotes.Note;
import com.example.branislavbily.mynotes.R;

import com.example.branislavbily.mynotes.DatabaseHelper;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 04.12.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */

public class NoteActivity extends AppCompatActivity {


    private DatabaseHelper myDB;
    private Note note;
    private Menu activityMenu;

    private int menuToChoose = R.menu.note_menu;

    private EditText editTextTitle, editTextDescription;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow);
        myDB = new DatabaseHelper(this);
        editTextTitle = findViewById(R.id.editTextNoteTitle);
        editTextDescription = findViewById(R.id.editTextNoteDescription);
        disableEditText(editTextDescription);
        disableEditText(editTextTitle);


        TextView textViewCreatedOn = findViewById(R.id.textViewCreatedOn);
        TextView textViewModifiedOn = findViewById(R.id.textViewModifiedOn);

        Intent intent = getIntent();
        String id = intent.getStringExtra(IntentModul.NOTE_ID);

        Cursor result = myDB.getNote(id);
        if(result.getCount() > 0) {
            result.moveToFirst();
            note = new Note(result.getString(0), result.getString(1),
                                 result.getString(2), result.getString(3), result.getString(4));
            editTextTitle.setText(note.getTitle());
            editTextDescription.setText(note.getDescription());
            textViewCreatedOn.append(note.getCreatedOn());
            textViewModifiedOn.append(note.getModifiedOn());
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        activityMenu = menu;
        getMenuInflater().inflate(menuToChoose, activityMenu);
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu, int menuId) {
        activityMenu = menu;
        activityMenu.clear();
        getMenuInflater().inflate(menuId, activityMenu);
        return true;
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.deleteOptionSelected) {
            deleteDialog();
        } else if(id == R.id.updateOptionSelected) {
            enableEditText(editTextTitle);
            enableEditText(editTextDescription);
            changeMenus();
        } else if(id == R.id.saveOptionSelected) {
            saveNote();
            changeMenus();
        }
        return super.onOptionsItemSelected(item);
    }

    private void disableEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setCursorVisible(false);
    }

    private void enableEditText(EditText editText) {
        editText.setFocusableInTouchMode(true);
        editText.setCursorVisible(true);
    }


    private void deleteDialog() {
        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        TextView title = createTitle();
        alertDialog.setCustomTitle(title);

        TextView msg = createMessage();
        alertDialog.setView(msg);

        setNeutralButton(alertDialog);
        setNegativeButton(alertDialog);

        new Dialog(getApplicationContext());
        alertDialog.show();
    }

    private void setNeutralButton(final AlertDialog alertDialog) {
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,"NO", (dialog, which) -> alertDialog.cancel());
    }

    private void setNegativeButton(AlertDialog alertDialog) {
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,"YES", (dialog, which) -> {
            if(myDB.deleteNote(note.getId())) {
                finish();
            } else {
                Toast.makeText(this, "Error while deleting note", Toast.LENGTH_SHORT).show();
            }

        });
    }

    private TextView createTitle() {
        TextView title = new TextView(this);
        title.setText(R.string.delete);
        title.setPadding(10, 10, 10, 10);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.BLACK);
        title.setTextSize(25);
        return title;
    }

    private TextView createMessage() {
        TextView msg = new TextView(this);
        msg.setText(R.string.wont_undo);
        msg.setGravity(Gravity.CENTER_HORIZONTAL);
        msg.setTextColor(Color.BLACK);
        msg.setTextSize(20);
        return msg;
    }

    private void changeMenus() {
        if(activityMenu != null) {
            if(menuToChoose == R.menu.note_menu) {
                onCreateOptionsMenu(activityMenu, R.menu.note_menu_update);
            } else {
                onCreateOptionsMenu(activityMenu, R.menu.note_menu);
            }
        }
    }

    private void saveNote() {
        String title = editTextTitle.getText().toString();
        String description = editTextDescription.getText().toString();
        if(myDB.updateNote(note.getId(),title, description)) {
            finish();

        } else {
            Toast.makeText(this, "Could not change values!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
