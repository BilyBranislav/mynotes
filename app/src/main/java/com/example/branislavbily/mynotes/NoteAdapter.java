package com.example.branislavbily.mynotes;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 04.12.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteHolder> {

    private Context context;
    private int resource;
    private OnNoteClickListener onNoteClickListener;
    private ArrayList<Note> notes;

    public NoteAdapter(OnNoteClickListener onNoteClickListener, ArrayList<Note> notes) {
        super();
        this.onNoteClickListener = onNoteClickListener;
        this.notes = notes;
    }

    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.note_item, viewGroup, false);
        return new NoteHolder(v, onNoteClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteHolder noteHolder, int i) {
        noteHolder.setData(notes.get(i));
    }


    @Override
    public int getItemCount() {
        return notes.size();
    }

    class NoteHolder extends RecyclerView.ViewHolder {

        private View itemView;
        private OnNoteClickListener onNoteClickListener;

        private TextView title;
        private TextView description;
        private TextView dates;


        NoteHolder(@NonNull View itemView, OnNoteClickListener onNoteClickListener) {
            super(itemView);
            this.itemView = itemView;
            this.onNoteClickListener = onNoteClickListener;
        }

        void setData(final Note note) {
            title = itemView.findViewById(R.id.textViewTitle);
            description = itemView.findViewById(R.id.textViewDescription);
            dates = itemView.findViewById(R.id.textViewDates);
            title.setText(note.getTitle());
            description.setText(note.getDescription());
            String dateText = "Created on: " + note.getCreatedOn() + " / Modified on: " + note.getModifiedOn();
            dates.setText(dateText);

            itemView.setOnClickListener(v -> onNoteClickListener.onNoteClick(note.getId()));
        }
    }

    public interface OnNoteClickListener {
         void onNoteClick(String noteId);
    }
}
