package com.example.branislavbily.mynotes;

import android.provider.BaseColumns;

/**
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * and will be punished
 * This code is proprietary and confidential of the person stated bellow
 * Created by branislavbily on 04.12.2018
 * If you are confused, feel free to ask me <branislav.bily@gmail.com>
 */

public interface NoteModul {
    interface Note extends BaseColumns {

        String DATABASE_NAME = "MyNotes.db";

        String TABLE_NAME = "notes_table";

        String TITLE = "title";

        String DESCRIPTION = "description";

        String CREATED_ON = "createdOn";

        String MODIFIED_ON = "modifiedOn";
    }
}