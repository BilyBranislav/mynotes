MyNotes

Simple android application, that allows you to save your notes. Your notes are saved internally, so internet access is not mandatory.

Why use MyNotes

MyNotes is good at saving notes. Nothing more. It does not want your email, syncing to cloud etc. Simply save your notes.

Sorting

Basing sorting feature that allows you sort all your notes.

Updating and Deleting

Easily update and delete your notes permanently.
